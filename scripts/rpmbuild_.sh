#!/bin/bash 
# Specific scripts/rpmbuild_.sh for frontier-squid
# To be run before running the generic scripts/rpmbuild_.sh 
# Run this from this directory, scripts,

source ../../scripts/utilities.sh

specDefine2var release4source

cd ../SOURCES

echo "Version: ${Version}"
echo "release4source: ${release4source}"

wget "http://frontier.cern.ch/dist/frontier-squid-${Version}-${release4source}.tar.gz"

